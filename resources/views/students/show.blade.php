@extends('layout/template')
@section('content')
    <h3>Details Of Student</h3>
    <br>
    <form class="form-horizontal">
       
        <div class="form-group">
      <label for="Id" class="col-sm-2 control-label">ID</label>
      <div class="col-sm-10">
      
      <input type="text" class="form-control" id="id" placeholder={{$student->id}} readonly>
      
      </div>
        </div>
    

        <div class="form-group">
            <label for="Name" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" placeholder={{$student->name}} readonly>
            </div>
        </div>

        <div class="form-group">
            <label for="Department" class="col-sm-2 control-label">Department</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="dep" placeholder={{$student->department}} readonly>
            </div>
        </div>
        

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <a href="{{ url('students')}}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </form>
@stop