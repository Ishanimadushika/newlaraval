 
@extends('layout/template')

@section('content')
 <h3>Student Management System</h3>
 <a href="{{url('/students/create')}}" class="btn btn-success">Register New +</a>
 <hr>
 <table class="table table-striped table-bordered table-hover">
     <thead>
     <tr class="bg-info">
         <th>Id</th>
         <th>Name</th>
         <th>Department</th>
         
         <th colspan="3">Actions</th>
     </tr>
     </thead>
     <tbody>
     @foreach ($students as $student)
         <tr>
             <td>{{ $student->id }}</td>
             <td>{{ $student->name }}</td>
             <td>{{ $student->department }}</td>
             
             <td><a href="{{url('students',$student->id)}}" class="btn btn-primary">Read</a></td>
             <td><a href="{{route('students.edit',$student->id)}}" class="btn btn-warning">Update</a></td>
             <td>
             {!! Form::open(['method' => 'DELETE', 'route'=>['students.destroy', $student->id]]) !!}
             {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
             {!! Form::close() !!}
             </td>
         </tr>
     @endforeach

     </tbody>

 </table>
@endsection