 
@extends('layout.template')
@section('content')
    <h1>Update Student Details</h1>
    {!! Form::model($student,['method' => 'PATCH','route'=>['students.update',$student->id]]) !!}
    
    <div class="form-group">
        {!! Form::label('Name', 'Name:') !!}
        {!! Form::text('name',null,['class'=>'form-control']) !!}
    </div>
    @if (count($errors) > 0 && isset($errors->get('name')[0]))
    <div class="alert alert-danger">    
        <ul>       
     <li><?php echo($errors->get('name')[0])?></li>
        </ul>
    </div>
    @endif

    <div class="form-group">
        {!! Form::label('Department', 'Department:') !!}
        {!! Form::text('department',null,['class'=>'form-control']) !!}
    </div>
    @if (count($errors) > 0 && isset($errors->get('department')[0]))
    <div class="alert alert-danger">    
        <ul>       
     <li><?php echo($errors->get('department')[0])?></li>
        </ul>
    </div>
    @endif
   
    <div class="form-group">
        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
    </div>
    {!! Form::close() !!}
@stop